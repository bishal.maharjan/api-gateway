# api-gateway

API Gateway NodeJS

- main
    - main api-gateway made in NodeJS - Typescript
    - added express-http-proxy for for routing
    - contains authentication using MongoDB for authentication
    - idea:
        API Endpoint -> Authentication + Middlewares -> multiple other endpoints connecting to different services (written in different language)