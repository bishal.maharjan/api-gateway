import { Router, Request, Response } from 'express';
import middlewares from '../middlewares';
import httpProxy from 'express-http-proxy';
const route = Router();

const userServiceProxy = httpProxy('http://localhost:8080');

export default (app: Router) => {
  app.use('/test', route);

  route.get('/test', middlewares.isAuth, middlewares.attachCurrentUser, (req: Request, res: Response) => {
    return userServiceProxy(req, res);
  });
};
